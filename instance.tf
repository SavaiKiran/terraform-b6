provider "aws" {
  region = "us-east-1"
}

data "aws_subnet" "my_subnet" {
  availability_zone = "us-east-1e"
  vpc_id = "vpc-0c432a9563f445690"
}

resource "aws_instance" "my_instance" {
#  ami = "${var.image_id}"
  ami = var.image_id
  tags = {
    Name = "cloudblitz-instance"
  }
  vpc_security_group_ids = ["sg-09098ef6e9d15412b", aws_security_group.allow_http.id]
  instance_type = var.instance_type
  key_name = "cbz-demo"
  subnet_id = data.aws_subnet.my_subnet.id
}

resource "aws_security_group" "allow_http" {
  name        = "allow-http port"
  description = "Allow http inbound traffic"
  vpc_id      = "vpc-0c432a9563f445690"

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

variable "image_id" {
  description = "My Image ID"
  default = "ami-053b0d53c279acc90"
}

variable "instance_type" {
  description = "Instance Type"
  default = "t2.micro"
}